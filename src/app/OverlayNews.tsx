import Image from "next/image";

interface OverlayNewsInt {
    news: any;
    closeOverlay: () => void;
}

export const OverlayNews: React.FC<OverlayNewsInt> = ({ news, closeOverlay }) => {

    const body = document.body;
    body.classList.add('modal-open');

    return (
        <div className="fixed top-0 left-0 right-0 bottom-0 w-full h-full bg-[#051B39] md:bg-black/[.8] z-[100]">
            <button onClick={closeOverlay}  className="block md:hidden absolute left-[18px] top-[17px] block w-[24px] h-[24px] z-[10]">
                <Image fill sizes="100%" alt="" src="/icons/icon-left.svg" />
            </button>
            <div className="absolute top-[56px] md:top-0 left-0 right-0 bottom-0 m-auto w-full h-auto md:w-[750px] md:h-[395px] xl:w-[1200px] xl:h-[632px]">
                <div className="relative z-[10] w-full h-full overflow-auto md:overflow-hidden px-[15px] py-[22px] md:p-[50px] xl:p-[80px] md:flex md:justify-between">
                    <button onClick={closeOverlay} className="hidden md:block absolute top-[40px] right-[40px] xl:right-[57px] w-[30px] h-[30px] xl:w-[50px] xl:h-[50px] z-[20]">
                        <Image fill sizes="100%" src={'icons/icon-close.svg'} alt="icon" style={{ objectFit: "cover" }} />
                    </button>
                    
                    { news.image_url && (
                        <div className="relative w-full h-[231px] md:h-[320px] md:w-[310px] xl:h-[492px] mb-[20px] md:mb-0 border-[4px] rounded-[16px] overflow-hidden">
                            <Image
                                fill
                                sizes="100%"
                                alt="donwload"
                                src={news.image_url}
                                style={{
                                    objectFit: "cover",
                                }}
                            />
                        </div>
                    )}
                    <div className={`w-full ${news.image_url ? 'md:w-[680px]' : ''} md:pl-[30px]`}>
                        <h4 className="text-white text-[26px] leading-[26px] xl:text-[40px] xl:leading-[40px] mb-[15px] font-arialnarrow font-bold">{news.title_es}</h4>
                        <div className="h-full max-h-[280px] xl:max-h-[400px] overflow-auto">
                            <p className="text-[16px] leading-[24px] font-arialnarrow xl:text-[24px] xl:leading-[36px] mb-[5px]">{ news.content_es }</p>
                        </div>
                    </div>

                </div>
                <Image
                    fill
                    sizes="100%"
                    className="hidden md:block"
                    alt="overlay"
                    src="/overlay/bg-overlay-2.png"
                    style={{
                        objectFit: "cover",
                    }}
                />
                <Image
                    fill
                    sizes="100%"
                    className="block md:hidden"
                    alt="overlay"
                    src="/overlay/bg-overlay-3.png"
                    style={{
                        objectFit: "cover",
                    }}
                />
            </div>
        </div>
    )
}
