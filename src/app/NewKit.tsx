import Image from "next/image";

export const NewKit: React.FC = () => {
  return (
    <div className="relative">
      <div className="absolute left-0 right-0 bottom-[115px] md:bottom-[8rem] md:leading-[56px] text-center font-syncopate font-normal text-[24px] md:text-[42px] z-[10] text-white">
        GANATE EL <br /> NUEVO KIT
      </div>

      <div className="absolute left-0 right-0 bottom-[86px] text-center text-[14px] md:text-[14px] z-[10]">
        Temporada 23/24
      </div>

      <div className="absolute left-0 right-0 bottom-[34px] flex">
        <a href="#" className="text-sm bg-[#FF2850] px-12 py-2 z-[10] mx-auto rounded-[4px]">
          TAKE PART
        </a>
      </div>

      <div className="relative h-[274px] md:h-[630px] w-full">
        <div className="absolute left-0 right-0 top-[15px] md:top-[2rem] h-[23px] md:h-[58px] w-full cursor-pointer mx-auto z-[10]">
          <Image
            fill
            sizes="100vw"
            alt="donwload"
            src="/newkit/title.svg"
            style={{
              objectFit: "contain",
            }}
          />
        </div>

        <Image
          fill
          sizes="100vw"
          alt="donwload"
          src="/newkit/new-kit.png"
          style={{
            objectFit: "cover",
          }}
        />
      </div>
    </div>
  );
};
