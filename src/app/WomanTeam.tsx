import Image from "next/image";

export const WomanTeam: React.FC = () => {
  // TODO: No visible por el momento.
  return null;

  return (
    <div className="relative mt-[5rem]">
      <div className="relative h-[680px] w-full">
        <div className="text-[34px] text-center w-fit absolute left-0 right-0 top-[2rem] cursor-pointer ml-[4rem] z-[11]">
          PRIMER EQUIPO FEMENIL
        </div>

        <div className="absolute flex gap-4 top-[2rem] right-[10rem] z-[10]">
          <div className="relative w-[83px] h-[45px] cursor-pointer">
            <Image
              fill
              sizes="80vw"
              alt="carousels"
              src="/commons/arrow-left.svg"
            />
          </div>
          <div className="relative w-[83px] h-[45px] cursor-pointer">
            <Image
              fill
              sizes="80vw"
              alt="carousels"
              src="/commons/arrow-right.svg"
            />
          </div>
        </div>

        <div className="flex absolute h-[460px] left-0 right-0 justify-center top-[8rem] gap-2 z-[10]">
          <Image
            height={480}
            width={480}
            sizes="100vw"
            alt="donwload"
            src="/players/w-player1.jpeg"
            style={{
              objectFit: "cover",
            }}
          />
          <Image
            height={480}
            width={480}
            sizes="100vw"
            alt="donwload"
            src="/players/w-player2.jpeg"
            style={{
              objectFit: "cover",
            }}
          />
          <Image
            height={480}
            width={480}
            sizes="100vw"
            alt="donwload"
            src="/players/w-player3.jpeg"
            style={{
              objectFit: "cover",
            }}
          />
        </div>

        <Image
          fill
          sizes="100vw"
          alt="donwload"
          src="/players/bg-woman-team.jpeg"
          style={{
            objectFit: "cover",
          }}
        />
      </div>
    </div>
  );
};
