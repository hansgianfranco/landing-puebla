import type {Config} from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        chakrapetch: ["var(--font-chakra-petch)"],
        syncopate: ["var(--font-syncopate)"]
      },
    },
  },
  plugins: [],
};
export default config;
