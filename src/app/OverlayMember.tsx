import Image from "next/image";
import moment from 'moment';
import 'moment/locale/es';

interface OverlayMemberInt {
    member: any;
    closeOverlay: () => void;
}

export const OverlayMember: React.FC<OverlayMemberInt> = ({ member, closeOverlay }) => {

    const body = document.body;
    body.classList.add('modal-open');

    const formateDate = (date: string) => {
        return moment(date).locale('es').format('DD - MM -YYYY');
    };

    return (
        <div className="fixed top-0 left-0 right-0 bottom-0 w-full h-full bg-[#051B39] md:bg-black/[.8] z-[100]">
            <button onClick={closeOverlay}  className="block md:hidden absolute left-[18px] top-[17px] block w-[24px] h-[24px] z-[10]">
                <Image fill sizes="100%" alt="" src="/icons/icon-left.svg" />
            </button>
            <div className="absolute top-[56px] md:top-0 left-0 right-0 bottom-0 m-auto w-full h-auto md:w-[750px] md:h-[420px] xl:w-[1200px] xl:h-[650px]">
                <div className="relative z-[10] w-full h-full overflow-auto md:overflow-hidden">
                    <button onClick={closeOverlay} className="hidden md:block absolute top-[40px] right-[40px] xl:right-[57px] w-[30px] h-[30px] xl:w-[50px] xl:h-[50px] z-[20]">
                        <Image fill sizes="100%" src={'icons/icon-close.svg'} alt="icon" style={{ objectFit: "cover" }} />
                    </button>

                    <div className="absolute top-[34px] left-[28px]  md:top-[60px] md:left-[50px] xl:top-[91px] xl:left-[73px] z-[2]">
                        <p className="text-[24px] xl:text-[40px] mb-[5px]">{ (member.shirt ?? "") + " " + member.name + " " + member.last_name}</p>
                        <p className="text-[12px] xl:text-[20px] text-[#00D3F7] font-bold">{ member.position}</p>
                    </div>

                    <div className="absolute top-[436px] left-0 right-0 mx-auto w-[357px] h-[136px] md:mx-[inherit] md:right-[inherit] md:top-[inherit] md:bottom-[120px] md:left-[50px] xl:bottom-[220px] xl:left-[73px] xl:w-[438px] xl:h-[200px] py-[17px] px-[30px] z-[10] bg-black/[0.6] rounded-xl shadow border border-[#00D3F7]">
                        <h4 className="text-[#00D3F7] text-[24px] xl:text-[28px] font-bold font-['Roboto'] leading-[30px] xl:leading-10">Datos Personales:</h4>
                        <p className="text-white text-[16px] xl:text-xl font-roboto font-bold leading-[25px] xl:leading-10">Fecha de nacimiento: { formateDate(member.birthdate) ?? "-" }</p>
                        {/* <p className="text-white text-xl font-roboto font-bold leading-10">Nacionalidad:  Colombiana</p> */}
                        <p className="text-white text-[16px] xl:text-xl font-roboto font-bold leading-[25px] xl:leading-10">Estatura:  { member.height ? member.height + "cm" : "-"}</p>
                        <p className="text-white text-[16px] xl:text-xl font-roboto font-bold leading-[25px] xl:leading-10">Peso: { member.weight ? member.weight + "kg" : "-"}</p>
                    </div> 

                    <div className="absolute top-[83px] left-0 right-0 mx-auto md:top-[inherit] md:left-[inherit] md:mx-[inherit] md:bottom-[30px] md:right-[60px] w-[261px] h-[353px] md:w-[250px] md:h-[340px] xl:w-[390px] xl:h-[557px]">
                        <Image
                            fill
                            sizes="100%"
                            alt="donwload"
                            src={member.image_url ? member.image_url : "/players/man-player-default.png"}
                            style={{
                                objectFit: "cover",
                            }}
                        />
                    </div>

                    <div className="absolute flex justify-center md:justify-start gap-[10px] left-0 right-0 xl:right-[inherit] md:gap-[20px] top-[597px] md:top-[inherit] md:bottom-[50px] md:left-[50px] xl:bottom-[70px] xl:left-[73px]">
                        <div className="w-[112px] h-[51px] xl:w-[220px] xl:h-[100px] pt-[6px] xl:pt-[13px] bg-white rounded-[10px] shadow">
                            <p className="text-center text-[#1F4783] text-[9px] xl:text-lg font-black uppercase">Partidos Jugados</p>
                            <p className="text-center text-[#1F4783] text-[20px] xl:text-[41px] font-black uppercase">{ member.games_played ?? "-" }</p>
                        </div>  
                        <div className="w-[112px] h-[51px] xl:w-[220px] xl:h-[100px] pt-[6px] xl:pt-[13px] bg-white rounded-[10px] shadow">
                            <p className="text-center text-[#1F4783] text-[9px] xl:text-lg font-black uppercase">Goles</p>
                            <p className="text-center text-[#1F4783] text-[20px] xl:text-[41px] font-black uppercase">{ member.goals ?? "-" }</p>
                        </div>  
                        <div className="w-[112px] h-[51px] xl:w-[220px] xl:h-[100px] pt-[6px] xl:pt-[13px] bg-white rounded-[10px] shadow">
                            <p className="text-center text-[#1F4783] text-[9px] xl:text-lg font-black uppercase">Asistencias</p>
                            <p className="text-center text-[#1F4783] text-[20px] xl:text-[41px] font-black uppercase">{ member.assistance ?? "-" }</p>
                        </div>  
                    </div>
                    
                </div>
                <Image
                    fill
                    sizes="100%"
                    className="hidden md:block"
                    alt="overlay"
                    src="/overlay/bg-overlay-2.png"
                    style={{
                        objectFit: "cover",
                    }}
                />
                <Image
                    fill
                    sizes="100%"
                    className="block md:hidden"
                    alt="overlay"
                    src="/overlay/bg-overlay-3.png"
                    style={{
                        objectFit: "cover",
                    }}
                />
            </div>
        </div>
    )
}
