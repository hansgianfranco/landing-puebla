import Image from "next/image";
import { useEffect, useRef } from "react";

export const Header: React.FC = () => {

  const videoRef = useRef<HTMLVideoElement>(null);
  useEffect(() => {
    const handleAutoPlay = () => {
      if (videoRef.current) {
        videoRef.current.autoplay = true;
        videoRef.current.muted = true;
      }
    };

    handleAutoPlay();

    const handleClick = () => {
      if (videoRef.current && videoRef.current.paused) {
        videoRef.current.muted = false;
        videoRef.current.play();
      }
    };

    document.addEventListener('click', handleClick);

    return () => {
      document.removeEventListener('click', handleClick);
    };
  }, []);

  return (
    <div>
      <div className="relative overflow-hidden h-[375px] md:h-[680px] after:absolute after:w-full after:h-full after:top-0 after:bottom-0 after:left-0 after:right-0 after:bg-gradient-to-b after:from-transparent after:to-black/[.2]">
        <video ref={videoRef} width="400" height="300" className="absolute top-0 left-0 bottom-0 right-0 min-w-full min-h-full hidden md:block" autoPlay={true} muted poster="/headers/header1.jpeg">
          <source src="https://whiz-file-dev-25-01-22.s3.us-east-2.amazonaws.com/header-puebla.mp4" type="video/mp4" />
        </video>
        <Image 
          fill 
          alt="logos" 
          sizes="100%" 
          className="block md:hidden" 
          src="/headers/header2.png"
          style={{
            objectFit: "cover",
          }}
        />
      </div>
    </div>
  );
};
