import Image from "next/image";

export const DownloadApp: React.FC = () => {
  return (
    <div className="relative mb-[60px] md:mb-[120px]">
      <div className="relative max-w-[1230px] mx-auto">
        <div className="lg:absolute lg:right-[73px] lg:top-[2rem] mb-[20px] lg:mb-0 text-center lg:text-left text-[16px] md:text-[34px] tracking-wider font-bold">
          ¡DESCARGA LA APP DEL PUEBLA FC!
        </div>

        <div className="relative h-[550px] w-full">
          <Image
            fill
            className="hidden md:block"
            sizes="80vw"
            alt="donwload"
            src="/commons/download.png"
            style={{
              objectFit: "cover",
            }}
          />

          <Image
            fill
            className="md:hidden"
            sizes="80vw"
            alt="donwload"
            src="/commons/download2.png"
            style={{
              objectFit: "cover",
            }}
          />
          <a href="#" className="hidden lg:block absolute right-0 top-[260px] h-[100px] w-[300px] cursor-pointer">
            <Image
              fill
              sizes="100vw"
              alt="donwload"
              src="/commons/playstore.png"
              style={{
                objectFit: "contain",
              }}
            />
          </a>
          <a href="#" className="hidden lg:block absolute right-0 top-[350px] h-[100px] w-[300px] cursor-pointer">
            <Image
              fill
              sizes="100vw"
              alt="donwload"
              src="/commons/appstore.png"
              style={{
                objectFit: "contain",
              }}
            />
          </a>
        </div>
        <div className="block lg:hidden flex">
          <a href="#" className="w-1/2 relative">
            <Image
              fill
              className="!relative"
              sizes="100vw"
              alt="donwload"
              src="/commons/playstore.png"
              style={{
                objectFit: "contain",
              }}
            />
          </a>
          <a href="#" className="w-1/2 relative">  
            <Image
              fill
              className="!relative"
              sizes="100vw"
              alt="donwload"
              src="/commons/appstore.png"
              style={{
                objectFit: "contain",
              }}
            />
          </a>
        </div>
      </div>
    </div>
  );
};
