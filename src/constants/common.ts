export const apiUrl = process.env.API_URL || 'https://ml9c4hwuh4.execute-api.us-east-2.amazonaws.com/dev';

// @ts-ignore
export const fetcher = (...args) => fetch(...args).then((res) => res.json());
