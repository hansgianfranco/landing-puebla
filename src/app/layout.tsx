import type {Metadata} from "next";
import {Chakra_Petch, Syncopate} from "next/font/google";
import "./globals.css";

const chakraPetch = Chakra_Petch({
  display: 'swap',
  style: ["normal"],
  subsets: ["latin"],
  weight: ["400", "600", "700"],
  variable: '--font-chakra-petch'
})
const syncopate = Syncopate({
  display: 'swap',
  style: ["normal"],
  subsets: ["latin"],
  weight: ["700"],
  variable: '--font-syncopate'
})

export const metadata: Metadata = {
  title: "Puebla FC",
  description: "",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={`${chakraPetch.variable} ${syncopate.variable}`}>{children}</body>
    </html>
  );
}
