import Image from "next/image";
import useSWR from "swr";
import type {img} from "@/constants";
import {apiUrl, fetcher} from "@/constants";

export const LogoBanners: React.FC = () => {

  
  const { data, error, isLoading } = useSWR(
    `${apiUrl}/api/sponsors`,
    fetcher,
  );

  if (error) return null; 
  if (isLoading) return "";

  return (
    <div className="order-2 md:order-1 overflow-hidden md:block bg-black py-2 px-5">
      <div className="max-w-[1290px] mx-auto">
        <div className="flex flex-wrap items-center md:flex-nowrap justify-between gap-[10px] md:gap-[20px] relative">
          {data?.data?.map((img: img) => (
            <a href={img.web_view_url} target="_blank" key={img.id} className="w-[40px] md:w-auto relative opacity-[0.4]">
              <Image
                alt="logos" 
                sizes="80vw"
                width={'200'}
                height={'100'}
                objectFit="cover"
                src={img.image_url}
              />
            </a>
          ))}
        </div>
      </div>
    </div>
  );
};
