import Image from "next/image";
import moment from 'moment';
import 'moment/locale/es';
import Link from "next/link";

interface OverlayMatchInt {
    match: any;
    closeOverlay: () => void;
}

export const OverlayMatch: React.FC<OverlayMatchInt> = ({ match, closeOverlay }) => {

    const body = document.body;
    body.classList.add('modal-open');

    const formateDate = (date: string) => {
        return moment(date).locale('es').format('dddd DD/MM/YYYY, HH:mm');
    };

    const getProgress = (time: number) => {
        let step = 0;
        switch (true) {
            case time >= 90:
                step = 8;
                break;
            case time >= 80:
                step = 7;
                break;
            case time >= 70:
                step = 6;
                break;
            case time >= 60:
                step = 5;
                break;
            case time >= 50:
                step = 4;
                break;
            case time >= 40:
                step = 3;
                break;
            case time >= 30:
                step = 2;
                break;
            case time >= 20:
                step = 1;
                break;
            case time >= 10:
                step = 0;
                break;
            default:
                step = 0;
                break;
        }
        return `/steps/${step}.svg`;
    };

    return (
        <div className="fixed top-0 left-0 right-0 bottom-0 w-full h-full bg-[#051B39] md:bg-black/[.8] z-[100]">
            <button onClick={closeOverlay}  className="block md:hidden absolute left-[18px] top-[17px] block w-[24px] h-[24px] z-[10]">
                <Image fill sizes="100%" alt="" src="/icons/icon-left.svg" />
            </button>
            <div className="absolute top-[56px] md:top-0 left-0 right-0 bottom-0 m-auto w-full h-auto md:w-[750px] md:h-[420px] xl:w-[1200px] xl:h-[650px]">
                <div className="relative z-[10] w-full h-full overflow-auto md:overflow-hidden ">
                    <div className="absolute top-[13px] md:top-[40px] left-0 right-0 m-auto w-[263px] h-[38px] xl:w-[528px] md:h-[68px]">
                        <Image width={528} height={68} src={'overlay/logo-liga.svg'} alt="icon" style={{ objectFit: "cover" }} />
                    </div>
                    <button onClick={closeOverlay} className="hidden md:block absolute top-[40px] right-[40px] xl:right-[57px] w-[30px] h-[30px] xl:w-[50px] xl:h-[50px] z-[20]">
                        <Image fill sizes="100%" src={'icons/icon-close.svg'} alt="icon" style={{ objectFit: "cover" }} />
                    </button>

                    <div className="absolute w-[344px] h-[70px] xl:w-[552px] xl:h-[112px] pt-[8px] xl:pt-[12px] top-[76px] md:top-[120px] xl:top-[180px] md:left-[23px] left-[-50px] xl:left-[37px] right-0 text-center z-[1]">
                        <p className="relative uppercase text-[18px] xl:text-[30px] leading-[28px] xl:leading-[45px] font-bold z-[10]">{match.stadium ?? "-"}</p>
                        <p className="relative capitalize text-[11px] xl:text-[16px] font-normal leading-[16px] z-[10]">{formateDate(match.date + " " + match.time)}</p>
                        <Image
                            fill
                            sizes="100%"
                            alt="carousels"
                            src={'overlay/title.svg'}
                            style={{
                                objectFit: "cover",
                            }}
                        />
                    </div>

                    <div className="absolute top-[182px] md:top-[100px] xl:top-[147px] w-[360px] xl:w-[564px] right-[30px] xl:right-[57px] xl:px-[30px] left-0 right-0 mx-auto md:mr-0 flex items-center justify-between">
                        {match.home_team?.shield_url ? (
                            <div className="max-w-[110px] w-full">
                                <div className="relative mx-auto h-[77px] w-[77px] xl:h-[85px] xl:w-[85px]">
                                    <Image
                                        fill
                                        sizes="100%"
                                        alt="carousels"
                                        src={match.home_team.shield_url}
                                        style={{
                                            objectFit: "cover",
                                        }}
                                    />
                                </div>
                                <div className="font-bold text-center text-[14px] leading-[27px]">
                                    {match.home_team?.name ?? "-"}
                                </div>
                                <div className="text-[#0FC8E4] text-center text-[10px] leading-[16px]">Local</div>
                            </div>
                        ) : null}

                        <div className="relative h-[126px] w-[126px] xl:h-[140px] xl:w-[140px]">
                            <div className="relative h-[126px] w-[126px] xl:h-[140px] xl:w-[140px]">
                                <Image
                                    fill
                                    sizes="100%"
                                    alt="carousels"
                                    src={
                                        match.isDisabled
                                            ? "/commons/progress-empty.svg"
                                            : getProgress(match.score?.update_minute)
                                    }
                                    style={{
                                        objectFit: "cover",
                                    }}
                                />
                            </div>
                            <div className="absolute top-[36px] left-0 right-0 text-center text-sm z-[10]">
                                {(match.score?.update_minute ?? "-") + "’"}
                            </div>
                            <div className="absolute top-0 left-0 right-0 bottom-0 m-auto h-[40px] text-center text-[30px] leading-[45px] z-[10] font-bold">
                                {match.score?.home_score ?? "0"} - {" "}
                                {match.score?.away_score ?? "0"}
                            </div>
                        </div>

                        {match.away_team?.shield_url ? (
                            <div className="max-w-[110px] w-full">
                                <div className="relative mx-auto h-[77px] w-[77px] xl:h-[85px] xl:w-[85px]">
                                    <Image
                                        fill
                                        sizes="100%"
                                        alt="carousels"
                                        src={match.away_team.shield_url}
                                        style={{
                                            objectFit: "cover",
                                        }}
                                    />
                                </div>
                                <div className="font-bold text-center text-[14px] leading-[27px]">
                                    {match.away_team?.name ?? "-"}
                                </div>
                                <div className="text-[#0FC8E4] text-center text-[10px] leading-[16px]">Visita</div>
                            </div>
                        ) : null}
                    </div>

                    <div className="absolute bottom-[40px] xl:bottom-[58px] max-w-[323px] xl:max-w-full top-[350px] md:top-[inherit] left-0 right-0 mx-auto md:ml-0 md:left-[55px] xl:left-[84px]">
                        <div className="relative pl-[18px] py-[8px] rounded-br-xl rounded-tr-xl xl:py-0 mb-[30px] md:mb-[20px] xl:mb-[40px] bg-[#000000]/[.4] md:bg-transparent after:absolute after:rounded-tl-xl after:rounded-bl-xl after:left-0 after:top-0 after:bottom-0 after:w-[6px] after:h-full after:bg-[#00D3F7]">
                            <p className="text-white italic text-[13px] xl:text-[21px] font-bold">Venta de boletos en taquillas del estadio</p>
                            <p className="text-neutral-400 text-[12px] xl:text-xl font-normal">Desde el lunes 7 de agosto a partir de la 1:00 pm</p>
                        </div>
                        <div className="relative pl-[18px] py-[8px] rounded-br-xl rounded-tr-xl xl:py-0 bg-[#000000]/[.4] md:bg-transparent after:absolute after:rounded-tl-xl after:rounded-bl-xl after:left-0 after:top-0 after:bottom-0 after:w-[6px] after:h-full after:bg-[#00D3F7]">
                            <p className="text-white italic text-[13px] xl:text-[21px] font-bold">Compra tus boletos online en <span className="inline-block ml-[6px] mt-[-11px] w-[98px] h-[13px] xl:w-[177px] xl:h-[24px] relative"><Image fill sizes="100%" src={'icons/icon-betamovil.svg'} alt="icon" style={{ objectFit: "cover" }} /></span></p>
                            <p className="text-neutral-400 text-[12px] xl:text-xl font-normal">Desde el jueves 10 de agosto a partir de la 11:00 am</p>
                        </div>
                    </div>

                    <Link href={match.ticket_url} className="w-[196px] h-[36px] xl:w-[347px] xl:h-[61px] rounded-[10px] flex items-center justify-center bg-[#FF2850] left-0 right-0 mx-auto xl:left-initial md:mr-0 md:right-[80px] xl:right-[147px] top-[530px] md:top-[inherit] md:bottom-[80px] xl:bottom-[122px] absolute text-center text-white text-[12px] xl:text-xl font-bold uppercase">
                        COMPRA TUS BOLETOS
                    </Link>
                </div>
                <Image
                    fill
                    sizes="100%"
                    className="hidden md:block"
                    alt="overlay"
                    src="/overlay/bg-overlay.png"
                    style={{
                        objectFit: "cover",
                    }}
                />
                <Image
                    fill
                    sizes="100%"
                    className="block md:hidden"
                    alt="overlay"
                    src="/overlay/bg-overlay-3.png"
                    style={{
                        objectFit: "cover",
                    }}
                />
            </div>
        </div>
    )
}
