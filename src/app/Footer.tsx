import Image from "next/image";
import Link from "next/link";

const socialNetworks = [
  { 
    img: "/commons/x.svg", 
    link: "https://twitter.com/MazatlanFC" 
  },
  {
    img: "/commons/fb.svg",
    link: "https://www.facebook.com/MazatlanFC/?locale=es_LA",
  },
  {
    img: "/commons/instagram.svg",
    link: "https://www.instagram.com/mazatlanfc/",
  },
  {
    img: "/commons/youtube.svg",
    link: "https://www.youtube.com/channel/UCm9E5Kxn7bS-nDvY1gNOnzg",
  },
  {
    img: "/commons/tiktok.svg",
    link: "https://www.tiktok.com/@mazatlanfc?lang=es",
  },
  {
    img: "/commons/linkedin.svg",
    link: "https://mx.linkedin.com/company/mazatlanfc",
  },
];

export const Footer: React.FC = () => {
  return (
    <div className="pt-[32px] md:pt-[98px] pb-[70px] md:pb-[35px] md:px-[30px] border-t border-[#3c3c3c]">
      <div className="w-full max-w-[1200px] mx-auto">
      <div className="w-full max-w-[1200px] mx-auto">
        <div className="border-b-[20px] px-[30px] md:px-0 border-b-white pb-8">
          <div className="md:flex items-center justify-between max-w-[314px] md:max-w-full mx-auto md:mx-0">

            <div className="relative max-w-[320px] md:max-w-[518px] lg:max-w-full mb-[30px] md:mb-0 mx-auto md:mx-0">
              <Image
                fill
                sizes="100%"
                alt="donwload"
                className="!relative"
                src="/newkit/title.png"
                style={{
                  objectFit: "contain",
                }}
              />
            </div>

            <div className="flex justify-center flex-wrap md:justify-start gap-[22px] lg:gap-[50px]">
              {socialNetworks.map((socialNetwork) => {
                return (
                  <Link
                    passHref
                    href={socialNetwork.img}
                    key={socialNetwork.link}
                  >
                    <Image
                      width={24}
                      height={24}
                      alt="donwload"
                      src={socialNetwork.img}
                      style={{
                        objectFit: "contain",
                      }}
                    />
                  </Link>
                );
              })}
            </div>
          </div>
        </div>
      </div>
      <div className="md:flex md:justify-end px-[30px] md:px-0 md:gap-[50px] pt-[15px] md:pt-[35px]">
        <div className="text-sm text-[#ABAAAA] my-2 mb-[32px] text-center md:hidden">Copyright © 2020 — 2023 Puebla F.C.</div>
        <div className="text-sm font-semibold text-[#ABAAAA] my-2">Términos y condiciones</div>
        <div className="text-sm font-semibold text-[#ABAAAA] my-2">Privacidad</div>
        <div className="text-sm font-semibold text-[#ABAAAA] my-2">Cookies</div>
        <div className="hidden text-sm font-semibold text-[#ABAAAA] my-2 md:block">Copyright © 2020 — 2023 Puebla F.C.</div>
      </div>
      </div>
    </div>
  );
};
